# Gerring started
### Setting container to draw canvas
```
let container = document.querySelector(".canvas__container")
```
### Sending container to constructor    
```
let graph = new GraphDraw(container)
```
### Creating params
```
let params = [];
```
Param example:
```
param = {
    name: String,
    coords: [[Int, Int],...],
    ?width: Int,
    ?color: String
}
```
### Sending params to API:
```
graph.setParams(params)
```