class GraphDraw {
    
    constructor(cont, params, options) {
        this.cont = cont;
        this.cnv = document.createElement("canvas");
        this.ctx = this.cnv.getContext("2d");

        this.paths = [];

        this.setOptions(options);
        this.setParams(params);

        this.cnv.innerHTML = this.options.noCanvasText;

        //setting events handlers
        this.setPopupHandler();
        this.setZoomHandler();
        this.resize();
        window.onresize = (e) => {this.resize(this.cont)};
        this.setScrollHandler();

        this.cont.appendChild(this.cnv);
    }

    /**
     *  Setting user options
     */
    setOptions(options = {}) {
        //setting default options

        let padding = {
            left: (options.padding && options.padding.left)?options.padding.left:5,
            right: (options.padding && options.padding.right)?options.padding.right:0,
            top: (options.padding && options.padding.top)?options.padding.top:5,
            bottom: (options.padding && options.padding.bottom)?options.padding.bottom:5,
            X: (options.padding && options.padding.X)?options.padding.X:0,
            Y: (options.padding && options.padding.Y)?options.padding.Y:0,
        }

        this.options = {
            zoomSensitivity: options.zoomSensitivity || 1,

            bgColor: options.bgColor || "white",
            
            xAxisColor: options.xAxisColor || "black",
            yAxisColor: options.yAxisColor || "black",
            xAxisWidth: options.xAxisWidth || 2,
            yAxisWidth: options.yAxisWidth || 2,

            xSubAxisColor: options.xSubAxisColor || "darkgray",
            ySubAxisColor: options.ySubAxisColor || "darkgray",
            xSubAxisWidth: options.xSubAxisWidth || 1,
            ySubAxisWidth: options.ySubAxisWidth || 1,

            defaultParamWidth: options.defaultParamWidth || 5,
            defaultParamName: options.defaultParamName || "Param", 

            noCanvasText: options.noCanvasText || "Sorry, your browser is not supported.",

            padding: padding,

            legendHeight: options.legendHeight || 40,
        };
    }

    setParams(params = []) {
        this.params = [];
        params.forEach(param => {
            if(!param.name) param.name = this.options.defaultParamName + this.params.length;
            if(!param.width) param.width = this.options.defaultParamWidth;
            if(!param.color) param.color = this.genColor();
            this.params.push(param);
        })

        this.countValuesInterval();
        this.redraw();
    }

    /**
     *  Setting event handlers
     */
    setPopupHandler() {
        this.cont.onmousemove = (e) => {
              let pathHighlighted = false;
              
              let container = this.cnv.getBoundingClientRect(),
                  x = e.clientX - container.left,
                  y = e.clientY - container.top;
              
              this.paths.forEach((p, i) => {
                if(this.ctx.isPointInPath(p, x, y)) {
                    pathHighlighted = true;
                    this.redraw(i, [x, y]);
                }
              })

              if(!pathHighlighted) this.redraw();
        };
    }
    
    setScrollHandler() {
        this.isScrolling = false;
        this.cont.onmousedown = (e) => {
            this.isScrolling = true;
            window.startPoint = e.clientX;
            this.marginXzoomBefore = this.marginXzoom;

            window.addEventListener("mousemove", this.swipeHandler)
            window.onmousemove = (e) => {
                if(this.isScrolling) {
                    let delta = this.marginXzoomBefore + window.startPoint - e.clientX,
                        maxDelta = this.intervalX * this.modX - this.width;

                    // marginXzoom = [0, maxDelta], checking if delta in interval
                    this.marginXzoom = Math.min(maxDelta, Math.max(0, delta));
                }
            }
        }
        window.onmouseup = (e) => {
            this.isScrolling = false;
            window.startPoint = null;
        }
    }

    setZoomHandler() {
        this.multXzoom = 0;
        this.marginXzoom = 0;
        this.cont.onmousewheel = (e) => {
            let isPlus = e.deltaY > 0;
            this.zoom(isPlus);
        }
    }

    zoom(isPlus) {
        // let sign = (isPlus)?1:-1;
        // this.multXzoom = Math.max(0, this.multXzoom + this.options.zoomSensitivity * sign);

        if(isPlus) {
            this.multXzoom += this.options.zoomSensitivity;
        } else {
            this.multXzoom = Math.max(0, this.multXzoom - this.options.zoomSensitivity);
        }

        let prevModX = this.modX;     
        this.countSizeProportions();
        let deltaX = this.intervalX * this.modX / prevModX;

        //right corner fix
        if(this.marginXzoom + this.width > this.intervalX * this.modX) this.marginXzoom -= deltaX * this.options.zoomSensitivity;

        if(isPlus) {
            this.marginXzoom += deltaX / (this.options.zoomSensitivity * 2);
        } else {
            this.marginXzoom = Math.max(0, this.marginXzoom - deltaX / (this.options.zoomSensitivity * 2));
        }
        // this.marginXzoom = Math.max(0, this.marginXzoom - this.intervalX * (this.modX / prevModX) / 2 * sign);
        
        // console.log(prevModX, this.modX, this.marginXzoom);

        this.redraw();
    }

    resize() {
        this.width = Math.floor(this.cont.offsetWidth * (100 - this.options.padding.left - this.options.padding.right) / 100);
        this.height = Math.floor(this.cont.offsetHeight * (100 - this.options.padding.top - this.options.padding.bottom) / 100) - this.options.legendHeight;  

        this.cnv.width = this.cont.offsetWidth;
        this.cnv.height = this.cont.offsetHeight; 

        this.paddingLeft = Math.floor(this.cont.offsetWidth * this.options.padding.left / 100); 
        this.paddingRight = Math.floor(this.cont.offsetWidth * this.options.padding.right / 100); 
        this.paddingTop = Math.floor(this.cont.offsetHeight * this.options.padding.top / 100);  
        this.paddingBottom = Math.floor(this.cont.offsetHeight * this.options.padding.bottom / 100); 

        this.countSizeProportions();
        this.redraw();
    }

    /**
     *  Draw functions
     */
    redraw(iHighligth, popupPos) {
        if(!this.width || !this.height) return;

        this.paths = [];

        this.drawBg();

        // ctx.transform(1, 0, 0, -1, 0, this.height);
        // ctx.scale(3, 1);

        // drawing lines
        this.params.forEach((param, index) => {
            let path = new Path2D(); // line path

            let ctx = this.ctx;
            
            ctx.beginPath();
            ctx.lineWidth = param.width;
            ctx.strokeStyle = (iHighligth == index) ? "red" : param.color;
            ctx.fillStyle = (iHighligth == index) ? "red" : param.color;

            param.coords.forEach(coord => {
                let X = this.paddingLeft + (coord[0] + this.intervalXbelow0) * this.modX - this.marginXzoom,
                    Y = this.height - (coord[1] + this.intervalYbelow0) * this.modY + this.paddingTop;

                // if(X) {
                    //line
                    ctx.lineTo(X, Y);    
                    ctx.stroke();
                    //dot
                    path.moveTo(X, Y);
                    path.arc(X, Y, 3, 0, 2 * Math.PI);
                // }

                // if(!path.coords) path.coords = {}
                // path.coords[coord[0]] = `(${coord[0]};${coord[1]})`;            
            });
            
            ctx.fill(path);
            this.paths.push(path);
        });

        // if(this.paths[iHighligth]) console.log(this.paths[iHighligth].coords);
        
        this.drawAxis();
        this.drawLegend();
        if(popupPos) this.drawPopup(popupPos);
    }

    drawPopup(coords) {
        let ctx = this.ctx,
            X = coords[0],
            Y = coords[1],
            Ycoord = Math.floor(this.intervalY - (Y - this.paddingTop) / this.modY - this.intervalYbelow0),
            Xcoord = Math.floor((this.marginXzoom + X - this.paddingLeft) / this.modX - this.intervalXbelow0),
            text = `(${Xcoord};${Ycoord})`,
            popupWidth = text.length * 7,
            popupHeight = 20,
            marginX = 2,
            marginY = -25;
        
        //prevent getting popup out of window
        // Y = Math.min(this.cont.width - popupWidth, Y);
        // X = Math.min(this.cont.height - popupHeight, X);
        if(X + popupWidth > this.width) marginX -= popupWidth;
        if(Y - popupHeight < this.paddingTop) marginY += popupHeight;

        ctx.fillStyle = "white";
        // ctx.strokeStyle = "red";
        // ctx.lineWidth = 1;
        ctx.fillRect(X+marginX,Y + marginY, popupWidth, popupHeight);
        // ctx.strokeRect(X+2,Y-25, 50, 20);

        ctx.font = "15px Comic Sans MS";
        ctx.textAlign = "left";
        ctx.fillStyle = "black";
        ctx.fillText(text, X+marginX+3, Y+marginY+13);

    }

    drawBg() {
        this.ctx.fillStyle = this.options.bgColor;
        this.ctx.fillRect(0, 0, this.cnv.width, this.cnv.height);
    }

    drawAxis() {
        let ctx = this.ctx;

        // working in coord system, not pixels
        let zoomLvl = Math.ceil(this.multXzoom / 40) * 2 || 1,
            stepXamount = 8 * zoomLvl,
            stepYamount = 5; 

        let stepX = Math.floor(this.intervalX / stepXamount),
            stepY = Math.floor(this.intervalY / stepYamount);

        // if(this.multXzoom > 30) stepX /= 2;
        
        let paddingX = this.intervalXbelow0 % stepX, 
            paddingY = this.intervalYbelow0 % stepY;

        ctx.textAlign = "center";
        ctx.font = "14px Comic Sans MS";
        ctx.fillStyle = "red";

        // Oy builds by steps on OX
        for(let axisX = paddingX; axisX <= this.intervalX; axisX += stepX) {
            ctx.beginPath();

            let isMainAxis = Math.abs(axisX - this.intervalXbelow0) == 0;
            
            ctx.lineWidth = (isMainAxis) ? this.options.xAxisWidth : this.options.xSubAxisWidth;
            ctx.strokeStyle = (isMainAxis) ? this.options.xAxisColor : this.options.xSubAxisColor;

            let X = this.paddingLeft + axisX * this.modX - this.marginXzoom;
            if(isMainAxis && X < this.paddingLeft) X = this.paddingLeft;

            ctx.moveTo(X, this.paddingTop);
            ctx.lineTo(X, this.paddingTop + this.height);
            ctx.stroke();   

            // drawing text OY
            if(isMainAxis) {
                for(let axisY = paddingY; axisY <= this.intervalY; axisY += stepY) {
                    let label = (axisY - this.intervalYbelow0),
                        textX = this.paddingLeft + (this.intervalXbelow0 * this.modX) - 15 - this.marginXzoom;
                    if(textX < this.paddingLeft - 15) textX = this.paddingLeft - 15;

                    if(label != 0)
                        ctx.fillText(
                            label, 
                            textX, 
                            this.height + this.paddingTop - (axisY * this.modY) + 5
                        );
                }
            } 
        }

        // Ox builds by steps on OY
        for(let axisY = paddingY; axisY <= this.intervalY; axisY += stepY) {
            ctx.beginPath();

            let isMainAxis = Math.abs(axisY - this.intervalYbelow0) == 0;

            ctx.lineWidth = (isMainAxis) ? this.options.yAxisWidth : this.options.ySubAxisWidth;
            ctx.strokeStyle = (isMainAxis) ? this.options.yAxisColor : this.options.ySubAxisColor;

            ctx.moveTo(this.paddingLeft - this.marginXzoom, this.height + this.paddingTop - (axisY * this.modY));
            ctx.lineTo(this.paddingLeft + (this.intervalX * this.modX) - this.marginXzoom, this.height + this.paddingTop - (axisY * this.modY));
            ctx.stroke();
            
            // drawing text OX
            if(isMainAxis) {
                for(let axisX = paddingX; axisX < this.intervalX; axisX += stepX) {
                    let label = (axisX - this.intervalXbelow0).toFixed(0),
                        zeroUniqueMod = (label == 0)?-10:0;
                    ctx.fillText(
                        label, 
                        this.paddingLeft + (axisX * this.modX) + zeroUniqueMod - this.marginXzoom, 
                        this.height + this.paddingTop - (this.intervalYbelow0 * this.modY) + 15
                    );
                }
            } 
        }
    }

    drawLegend() {
        let ctx = this.ctx;

        // ctx.strokeStyle = "black";
        // ctx.strokeRect(
        //     this.paddingLeft, 
        //     this.cnv.height - this.paddingBottom - this.options.legendHeight + 5, 
        //     this.width - this.paddingRight, 
        //     this.cnv.height - this.paddingBottom
        // );

        ctx.font = "24px Comic Sans MS";
        ctx.textAlign = "left";

        this.params.forEach((param, i) => {
            ctx.lineWidth = param.width + 2;
            ctx.strokeStyle = param.color;
            ctx.beginPath()
            ctx.moveTo(this.paddingLeft + (this.width / this.params.length * i), this.cnv.height - (this.paddingBottom / 2));
            ctx.lineTo(this.paddingLeft + (this.width / this.params.length * i) + 20, this.cnv.height - (this.paddingBottom / 2));
            ctx.stroke();

            ctx.fillStyle = param.color;
            ctx.fillText(param.name, 24 + this.paddingLeft + (this.width / this.params.length * i), 5 + this.cnv.height - (this.paddingBottom / 2));
        })
    }

    /**
     *  Support methods
     */
    countValuesInterval() {
        if(!this.width || !this.height) return;
        let minX, maxX, minY, maxY, minYfrom0, minXfrom0;
        this.params.forEach(param => {
            param.coords.forEach(coord => {
                let X = coord[0],
                    Y = coord[1];

                if((minX === undefined) || (X < minX)) minX = X;
                if((minY === undefined) || (Y < minY)) minY = Y;

                if((maxX === undefined) || (X > maxX)) maxX = X;
                if((maxY === undefined) || (Y > maxY)) maxY = Y;

                if((X < 0) && ((minXfrom0 === undefined) || (X < minXfrom0))) minXfrom0 = X;
                if((Y < 0) && ((minYfrom0 === undefined) || (Y < minYfrom0))) minYfrom0 = Y;
            });
        });
        if(minY > 0) minY = 0;
        if(minX > 0) minX = 0;
        
        //little padding in graph
        maxX = Math.ceil(maxX * (1 + this.options.padding.X / 100));
        maxY = Math.ceil(maxY * (1 + this.options.padding.Y / 100));

        this.intervalX = Math.abs(maxX-minX);
        this.intervalY = Math.abs(maxY-minY);
        this.intervalXbelow0 = Math.abs(minXfrom0) || 0;
        this.intervalYbelow0 = Math.abs(minYfrom0) || 0;

        this.countSizeProportions();
    }

    countSizeProportions() {
        // uses ONLY to convert param coords to canvas pixel coords
        this.modX = this.width / this.intervalX + this.multXzoom; 
        this.modY = this.height / this.intervalY;
    }

    genColor() {
        const LETTERS = '0123456789ABC'; //removed some letters to prevent light colors

        let color = '#';
        for (let i=0; i<6; i++) {
            color += LETTERS[Math.floor(Math.random() * LETTERS.length)];
        }
        return color;
    }

}

function generateParam(size = 400, maxY = 200, options = {}) {
    let newParam = {
        name: options.name || "param",
        coords: [],
        width: options.width || 1,
    }
    for(let i=0; i <= size; i++) {
        let sign = (Math.random() > 0.5) ? 1 : -1;
        newParam.coords.push([i - 3, (sign * Math.floor(Math.random() * maxY))])
    }
    return newParam;
}

function main() {
    let cnvCont = document.querySelector(".canvas__container"),
        graph = new GraphDraw(cnvCont);

    let params = [];

    /* 
    param1 = {
        name: String,
        coords: [[Int, Int],...],
        ?width: Int,
        ?color: String
    }
    */

    params.push(generateParam(350));
    params.push(generateParam(240));
    params.push(generateParam(380));
    params.push(generateParam());
    
    graph.setParams(params);
}

window.onload = main;